from .pgdb import PgDb
from datetime import datetime, timezone
from api.models import User

class PgUser(PgDb):
    def create_user(self, name, email, password):
        now = datetime.now(timezone.utc)
        sql = '''
        INSERT INTO users (name, email, password_hash, created_on)
        VALUES (%s, %s, %s, %s)
        RETURNING *
        '''
        hashed_pwd = User.hash_password(password)
        with self._cursor() as cur:
            cur.execute(sql, [name, email, hashed_pwd, now])
            row = cur.fetchone()
            user = User(**row)
        return user

    def get_user_by_user_id(self, user_id):
        sql = '''
        SELECT user_id, name, email, password_hash, created_on
        FROM users
        WHERE user_id = %s
        '''
        user = None
        with self._cursor() as cur:
            cur.execute(sql, [user_id])
            if cur.rowcount > 0:
                row = cur.fetchone()
                user = User(**row)
        return user

    def get_user_by_email(self, email):
        sql = '''
        SELECT user_id, name, email, password_hash, created_on
        FROM users
        WHERE email = %s
        '''
        user = None
        with self._cursor() as cur:
            cur.execute(sql, [email])
            if cur.rowcount > 0:
                row = cur.fetchone()
                user = User(**row)
        return user

    def get_user(self, user_id, email):
        sql = '''
        SELECT user_id, name, email, password_hash, created_on
        FROM users
        WHERE user_id = %s AND email = %s
        '''
        user = None
        with self._cursor() as cur:
            cur.execute(sql, [user_id, email])
            if cur.rowcount > 0:
                row = cur.fetchone()
                user = User(**row)
        return user

    def find_user(self, email, password):
        sql = '''
        SELECT user_id, name, email, password_hash, created_on
        FROM users
        WHERE email = %s
        '''
        user = None
        with self._cursor() as cur:
            cur.execute(sql, [email])
            if cur.rowcount > 0:
                row = cur.fetchone()
                user = User(**row)
        if user and user.check_password(password):
            return user
        else:
            return None
