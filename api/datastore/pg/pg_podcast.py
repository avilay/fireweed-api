from .pgdb import PgDb
from api.models import Podcast
from api.models import Episode


class PgPodcast(PgDb):
    def find_podcasts(self, search):
        pass

    def get_all_podcasts(self):
        pass

    def add_podcast(self, **kwargs):
        link = kwargs['link']
        title = kwargs['title']
        description = kwargs['description']
        author = kwargs['author']
        image_url = kwargs['image_url']
        feed_url = kwargs['feed_url']
        published_on = kwargs['published_on']
        episodes = kwargs['episodes']

        pc_ins = '''
        INSERT INTO podcasts (link, title, description, author, image_url, feed_url, published_on)
        VALUES (%s, %s, %s, %s, %s, %s, %s)
        RETURNING *
        '''
        with self._cursor() as cur:
            cur.execute(pc_ins, [link, title, description, author, image_url, feed_url, published_on])
            row = cur.fetchone()
            podcast = Podcast(**row)

        ep_ins = '''
        INSERT INTO episodes (podcast_id, title, published_on, description, podcast_url, image_url, web_url)
        VALUES (%s, %s, %s, %s, %s, %s, %s)
        '''
        with self._cursor() as cur:
            for episode in episodes:
                ep_title = episode['title']
                ep_pub_on = episode['published_on']
                ep_desc = episode['description']
                podcast_url = episode['podcast_url']
                image_url = episode['image_url']
                web_url = episode['web_url']
                params = [
                    podcast.podcast_id,
                    ep_title,
                    ep_pub_on,
                    ep_desc,
                    podcast_url,
                    image_url,
                    web_url
                ]
                cur.execute(ep_ins, params)

        return podcast
