CREATE TABLE IF NOT EXISTS users (
    user_id serial NOT NULL CONSTRAINT u_pri_key PRIMARY KEY,
    name varchar(50) NOT NULL,
    email varchar(50) NOT NULL UNIQUE,
    password_hash varchar(128) NOT NULL,
    created_on timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(2)
);


CREATE TABLE IF NOT EXISTS podcasts (
    podcast_id serial NOT NULL CONSTRAINT pd_pri_key PRIMARY KEY,
    link varchar(2048) NOT NULL,
    title varchar(128) NOT NULL,
    description text,
    author varchar(50),
    image_url varchar(2048),
    feed_url varchar(2048) NOT NULL,
    published_on timestamptz
);


CREATE TABLE IF NOT EXISTS episodes (
    episode_id serial NOT NULL CONSTRAINT ep_pri_key PRIMARY KEY,
    podcast_id integer NOT NULL CONSTRAINT fk_ep_podcasts REFERENCES podcasts ON DELETE CASCADE,
    title varchar(128) NOT NULL,
    published_on timestamptz,
    description text,
    podcast_url varchar(2048),
    image_url varchar(2048),
    web_url varchar(2048)
);


CREATE TABLE IF NOT EXISTS favorites (
    favorite_id serial NOT NULL CONSTRAINT f_pri_key PRIMARY KEY,
    user_id integer NOT NULL CONSTRAINT fk_f_user REFERENCES users ON DELETE CASCADE,
    podcast_id integer NOT NULL CONSTRAINT fk_f_podcast REFERENCES podcasts,
    added_on timestamptz NOT NULL
);

CREATE TABLE IF NOT EXISTS playlists (
    playlist_id serial NOT NULL CONSTRAINT pl_pri_key PRIMARY KEY,
    user_id integer NOT NULL CONSTRAINT fk_pl_user REFERENCES users ON DELETE CASCADE,
    episdoe_id integer NOT NULL CONSTRAINT fk_pl_episode REFERENCES episodes,
    added_on timestamptz NOT NULL,
    is_played boolean
);

CREATE TABLE IF NOT EXSISTS history (
    history_id serial NOT NULL CONSTRAINT h_pri_key PRIMARY KEY,
    user_id integer NOT NULL CONSTRAINT fk_h_user REFERENCES users,
    episode_id integer NOT NULL CONSTRAINT fk_h_episode REFERENCES episodes,
    played_on timestamptz NOT NULL
);