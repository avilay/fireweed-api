import logging
import psycopg2 as pg
import psycopg2.extras as pgextras


logger = logging.getLogger(__package__)

class PgDb:
    def __init__(self, config):
        self._pgconfig = config['POSTGRES']
        self._db = None
        self._connect()

    def _connect(self):
        self._db = pg.connect(**self._pgconfig)
        self._db.autocommit = True

    def close(self):
        if self._db and self._db.closed == 0:
            self._db.close()

    def _cursor(self):
        if self._db and self._db.closed != 0:
            logger.warn('PG connection is closed/broken. Re-connecting')
            self._connect()
        return self._db.cursor(cursor_factory=pgextras.RealDictCursor)
