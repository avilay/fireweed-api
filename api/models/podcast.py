class Podcast:
    def __init__(self, **kwargs):
        self.podcast_id = kwargs.get('podcast_id')
        self.link = kwargs.get('link')
        self.title = kwargs.get('title')
        self.description = kwargs.get('description')
        self.author = kwargs.get('author')
        self.image_url = kwargs.get('image_url')
        self.feed_url = kwargs.get('feed_url')
        self.published_on = kwargs.get('published_on')
        self.is_favorite = kwargs.get('is_favorite')

    def to_dict(self):
        return dict(
            podcast_id=self.podcast_id,
            link=self.link,
            title=self.title,
            description=self.description,
            author=self.author,
            image_url=self.image_url,
            feed_url=self.feed_url,
            published_on=str(self.published_on),
            is_favorite=self.is_favorite
        )

    def __repr__(self):
        if len(self.description) > 20:
            desc = self.description[:20] + ' ...'
        else:
            desc = self.description
        return f'<Podcast(podcast_id={self.podcast_id}, link={self.link}, title={self.title}, description={desc}, author={self.author}, image_url={self.image_url}, feed_url={self.feed_url}, published_on={self.published_on}, is_favorite={self.is_favorite})>'