from datetime import timezone
import bcrypt


class User:
    def __init__(self, **kwargs):
        self.user_id = kwargs.get('user_id')
        self.name = kwargs.get('name')
        self.email = kwargs.get('email')
        self.password_hash = kwargs.get('password_hash')
        self.created_on = kwargs.get('created_on')

    def check_password(self, password):
        password = password.encode('utf-8')
        password_hash = self.password_hash.encode('utf-8')
        return bcrypt.hashpw(password, password_hash) == password_hash

    def to_dict(self):
        return dict(
            user_id=self.user_id,
            name=self.name,
            email=self.email,
            password_hash=self.password_hash,
            created_on=self.created_on.astimezone(timezone.utc).isoformat()
        )

    def __repr__(self):
        return f'<User(user_id={self.user_id}, name={self.name}, email={self.email}, created_on={self.created_on}>'

    @staticmethod
    def hash_password(password):
        password = password.encode('utf-8')
        return bcrypt.hashpw(password, bcrypt.gensalt()).decode()
