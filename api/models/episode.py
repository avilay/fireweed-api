class Episode:
    def __init__(self, **kwargs):
        self.episode_id = kwargs.get('episode_id')
        self.title = kwargs.get('title')
        self.published_on = kwargs.get('published_on')
        self.description = kwargs.get('description')
        self.in_playlist = kwargs.get('in_playlist')
        self.podcast_url = kwargs.get('podcast_url')
        self.image_url = kwargs.get('image_url')
        self.web_url = kwargs.get('web_url')

    def to_dict(self):
        return dict(
            episdoe_id=self.episode_id,
            title=self.title,
            published_on=str(self.published_on),
            description=self.description,
            in_playlist=self.in_playlist,
            podcast_url=self.podcast_url,
            image_url=self.image_url,
            web_url=self.web_url
        )

    def __repr__(self):
        if len(self.description) > 20:
            desc = self.description[:20] + ' ...'
        else:
            desc = self.description
        return '<Episode(' + \
               f'episode_id={self.episode_id}, title={self.title}, published_on={self.published_on},' + \
               f'description={desc}, in_playlist={self.in_playlist},' + \
               f'podcast_url={self.podcast_url}, image_url={self.image_url}, web_url={self.web_url})>'
