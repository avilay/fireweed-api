import logging
from aiohttp import web
from podcastparser import PodcastFeed


logger = logging.getLogger(__package__)

class PodcastController:
    def __init__(self, db):
        self._podcast_db = db

    def setup_routes(self, app):
        app.router.add_route('GET', '/podcasts', self.get_podcasts)
        app.router.add_route('POST', '/podcasts', self.add_podcast)
        app.router.add_route('GET', '/podcasts/{podcast_id}', self.get_podcast)
        app.router.add_route('GET', '/podcasts/{podcast_id}/episodes', self.get_episodes)
        app.router.add_route('GET', '/podcasts/{podcast_id}/episodes/{episode_id}', self.get_episode)

    async def get_podcasts(self, req):
        search = req.GET.get('search')
        if search:
            podcasts = self._db.find_podcasts(search)
        else:
            podcasts = self._db.get_all_podcasts()
        podcasts_json = [podcast.to_dict() for podcast in podcasts]
        return web.json_response(data=podcasts_json)

    async def add_podcast(self, req):
        post_params = await req.json()
        feed_url = post_params.get('feed_url')
        podcast_feed = PodcastFeed.parse(feed_url)
        self._podcast_db.add_podcast(
            link=podcast_feed.link,
            title=podcast_feed.title,
            description=podcast_feed.description,
            author=podcast_feed.author,
            image_url=podcast_feed.image_url,
            feed_url=podcast_feed.feed_url,
            published_on=podcast_feed.published_on,
            episodes=podcast_feed.episodes
        )

    async def get_podcast(self, req):
        return web.json_response({'status': 'Under construction'})

    async def get_episodes(self, req):
        return web.json_response({'status': 'Under construction'})

    async def get_episode(self, req):
        return web.json_response({'status': 'Under construction'})
