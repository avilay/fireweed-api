import logging
import feedparser as fp
import dateutil.parser as dtparser
from urllib.parse import urlparse


logger = logging.getLogger(__package__)

class InvalidUrl(Exception):
    pass


class InvalidFeed(Exception):
    pass


class InvalidPodcast(Exception):
    pass


class Episode:
    def __init__(self, **kwargs):
        self.guid = kwargs.get('guid')
        self.title = kwargs.get('title')
        self.published_on = kwargs.get('published_on')
        self.description = kwargs.get('description')
        self.podcast_url = kwargs.get('podcast_url')
        self.image_url = kwargs.get('image_url')
        self.web_url = kwargs.get('web_url')

    def __repr__(self):
        if len(self.description) > 20:
            desc = self.description[:20] + ' ...'
        else:
            desc = self.description
        return f'<Episode(guid={self.guid}, title={self.title}, published_on={self.published_on}, description={desc}, podcast_url={self.podcast_url}, image_url={self.image_url}, web_url={self.web_url})>'


class PodcastFeed:
    def __init__(self, **kwargs):
        self.link = kwargs.get('link')
        self.title = kwargs.get('title')
        self.description = kwargs.get('description')
        self.author = kwargs.get('author')
        self.image_url = kwargs.get('image_url')
        self.feed_url = kwargs.get('feed_url')
        self.published_on = kwargs.get('published_on')
        self.episodes = kwargs.get('episodes')

    def __repr__(self):
        if len(self.description) > 20:
            desc = self.description[:20] + ' ...'
        else:
            desc = self.description
        return f'<PodcastFeed(link={self.link}, title={self.title}, description={desc}, author={self.author}, image_url={self.image_url}, feed_url={self.feed_url}, published_on={self.published_on}, num_episodes={len(self.episodes)})>'

    @classmethod
    def parse(cls, feed_url):
        logger.info(f'Parsing {feed_url}')

        if not feed_url:
            raise InvalidUrl(f'{feed_url} is not a valid url')

        url = urlparse(feed_url)
        if not url.scheme or not url.netloc:
            raise InvalidUrl(f'{feed_url} is not a valid url')

        feed = fp.parse(feed_url)
        if 'bozo_exception' in feed:
            raise InvalidFeed(str(feed.bozo_exception))

        if 'enclosures' not in feed.entries[0]:
            raise InvalidPodcast(f'{feed_url} does not seem to have any media')

        if not feed.entries[0]['enclosures'][0]['type'].startswith('audio'):
            raise InvalidPodcast(f'{feed_url} does not seem to have any audio')

        title = feed.feed.get('title')
        link = feed.feed.get('link')
        if feed.feed.get('summary'):
            description = feed.feed.get('summary')
        else:
            description = feed.feed.get('subtitle')
        if feed.feed.get('author'):
            author = feed.feed.get('author')
        else:
            author = feed.feed.get('credit')
        image_url = feed.feed.get('image')
        if image_url:
            image_url = image_url['href']
        published_on = feed.feed.get('published')
        if published_on:
            published_on = dtparser.parse(published_on)

        episodes = []
        for entry in feed.entries:
            guid = entry.id
            entry_published_on = entry.get('published_on')
            if entry_published_on:
                entry_published_on = dtparser.parse(published_on)
            entry_title = entry.get('title')
            entry_desc = entry.get('summary')
            entry_image_url = entry.get('image')
            if entry_image_url:
                entry_image_url = entry_image_url['href']
            entry_link = entry.get('link')
            podcast_url = None
            enclosures = entry.get('enclosures')
            if enclosures:
                podcast_url = entry.enclosures[0]['href']
            episode = Episode(
                guid=guid,
                published_on=entry_published_on,
                title=entry_title,
                description=entry_desc,
                image_url=entry_image_url,
                web_url=entry_link,
                podcast_url=podcast_url
            )
            episodes.append(episode)

        return PodcastFeed(
            link=link,
            title=title,
            description=description,
            author=author,
            image_url=image_url,
            feed_url=feed_url,
            published_on=published_on,
            episodes=episodes
        )




